output "loadbalancer_ip" {
  value = "${google_compute_address.lb-ip.address}"
}

output "health_check_link" {
  value = "${google_compute_http_health_check.lb-target-health-check.self_link}"
}
