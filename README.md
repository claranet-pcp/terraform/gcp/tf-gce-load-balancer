# tf-gce-load-balancer
Module for creating Load Balancer with accompanied 'autoscaling' group, static
external IP, firewall and DNS records for load balaner and zone apex pointing
into static IP.

It's meant to work in the way AWS ELB does.

# Variables
* `service` - will be used as a part of the created resources name, as a
  firewall tag, etc
* `envname` -
* `disk-image` -
* `zone_counter` - integer used to describe how many zones one want to utilize
  for backends
* `machine-type` ("g1-small") -
* `network-name` - network name into which firewall will be linked
* `subnetwork-name` - subnetwork name in which backends will be placed
* `dns-zone-name` - zone name to create records in
* `dns-zone-dns-name` - FQDN to be used in the records
* `startup-script` -
* `ip-forward` ("false") - are those instances act as natting boxes
* `firewall-tags` - list of tags to be applied on the instances
* `disk-device-name` ("sda") -
* `service-account-scopes` (["storage-ro", "compute-ro"])
* `lb-group-open-port` (["80", "443"]) -
* `lb-group-source-tags` - list of tags for traffic sources
* `lb-group-target-tags` - list of tags for traffic targets
* `lb-backend-health-check-interval` (5) -
* `lb-backend-health-check-timeout` (5) -
* `lb-backend-scaler-max` (1) -
* `lb-backend-scaler-min` (1) -
* `lb-backend-scaler-cooldown` (60) -
* `lb-backend-scaler-util-target` (0.6) -
* `lb-forwarding-rule-ports` ("80-9200") -
* `lb-dns-prefixes` - list of DNS prefixes to add to the LB IP (ex.uk) for
  uk.stage.gcp.lus.com
* `gce-region` ("europe-west1") -
* `gce-zones` ({europe-west1 = "europe-west1-b,europe-west1-c,europe-west1-d}) -

## Outputs

## Usage Example
```
module "varnish-lb-setup" {
  source = "../modules/tf-gce-load-balancer"

  service              = "varnish"
  envname              = "${var.envname}"
  disk-image           = "${var.disk_image}"
  machine-type         = "${var.varnish_machine_type}"
  zone_counter         = 3
  network-name         = "${data.terraform_remote_state.common.network_name}"
  subnetwork-name      = "${module.subnetwork.public_subnet_name}"
  dns-zone-name        = "${data.terraform_remote_state.common.dns_zone_name}"
  dns-zone-dns-name    = "${data.terraform_remote_state.common.dns_zone_dns_name}"
  startup-script       = "gs://${var.bootstrap_bucket}/lush-bootstrap.sh"
  firewall-tags        = ["varnish", "internal"]
  lb-group-source-tags = ["internal"]
  lb-group-target-tags = ["varnish"]
```
