/* Environment variables */
variable "service" {
  description = "The name of the service, will become the name prefix for resources created by this module"
  type = "string"
}

variable "envname" {
  description = "The value for this will be added to the networking tags, as well as the 'envname' label on the instance template"
  type = "string"
}

variable "gce-region" {
  description = "The GCP region in which to place the resources created by this module"
  type = "string"
  default = "europe-west1"
}

variable "gce-zones" {
  description = "A list of availability zones to span resources across"
  type = "string"
  default = {
    europe-west1 = "europe-west1-b,europe-west1-c,europe-west1-d"
  }
}

variable "zone_counter" {
  description = "Count of how many zones to distribute resources across"
  type = "string"
}

/* Instance template variables */
variable "disk-image" {
  description = "The disk image to use for the instance template, the name is also interpolated into the machine name"
  type = "string"
}

variable "network-name" {
  description = "The name of the network to connect resources to"
  type = "string"
}

variable "subnetwork-name" {
  description = "The name of the subnet to connect resources to"
  type = "string"
}

variable "startup-script" {
  description = "The startup script for instances built on the instance template"
  type = "string"
}

variable "ip-forward" {
  description = "Bool indicating whether IP forwarding is enabled on the instances created from the instance template"
  type = "string"
  default = false
}

variable "machine-type" {
  description = "The machine type to use when creating instances from the template"
  type = "string"
  default = "g1-small"
}

variable "firewall-tags" {
  description = "A list of additional networking tags to add to the instance template"
  type = "list"
}

variable "disk-device-name" {
  description = "The name of the storage device attached to instances created from the instance template"
  type = "string"
  default = "sda"
}

variable "service-account-scopes" {
  description = "A list of service account scopes to add to instances created from the template"
  type = "list"
  default = ["storage-ro", "compute-ro"]
}

/* DNS variables */
variable "dns-zone-name" {
  description = "The name of the GCP DNS zone to create records in for this load balancing service"
  type = "string"
}

variable "dns-zone-dns-name" {
  description = "The DNS suffix on A records to be created for the load balancer, will also be added to the 'domain' label on the instance template"
  type = "string"
}

/* Autoscale group/load balancer variables */
variable "lb-dns-prefixes" {
  description = "The list of DNS prefixes to add to the load balancer"
  type = "list"
}

variable "lb-group-open-port" {
  description = "List of ports to open on the load balancer"
  type = "string"
  default = ["80", "443"]
}

variable "lb-group-source-cidrs" {
  description = "The allowed list of CIDR blocks to connect to the load balancer"
  type    = "list"
  default = ["0.0.0.0/0"]
}

variable "lb-group-target-tags" {
  description = "A list of target tags to add to the load balanced instances firewall"
  type = "list"
}

variable "lb-backend-health-check-interval" {
  description = "Time (seconds) between http health checks on the load balanced instances"
  type = "string"
  default = 5
}

variable "lb-backend-health-check-timeout" {
  description = "Time (seconds) after which a connection attempt to a load balanced instance times out"
  type = "string"
  default = 5
}

variable "lb-backend-scaler-max" {
  description = "The minimum desired count of instances to be maintained by the autoscale group"
  type = "string"
  default = 1
}

variable "lb-backend-scaler-min" {
  description = "The maximum desired count of instances to be maintained by the autoscale group"
  type = "string"
  default = 1
}

variable "lb-backend-scaler-cooldown" {
  description = "The time (seconds) before another autoscaling event can occur"
  type = "string"
  default = 60
}

variable "lb-backend-scaler-util-target" {
  description = "The CPU utilisation target for autoscale events"
  type = "string"
  default = 0.6
}

variable "lb-forwarding-rule-ports" {
  description = "Range of ports that the load balancer can forward"
  type = "string"
  default = "80-9200"
}