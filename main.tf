/*
 * Module creating GCE Load Balancer (something similar to AWS ELB) including
 * networkk firewall
 */
resource "google_compute_firewall" "lb-instances-firewall" {
  name    = "${var.service}-firewall-${var.gce-region}"
  network = "${var.network-name}"

  allow {
    protocol = "tcp"
    ports    = ["${var.lb-group-open-port}"]
  }

  source_ranges = ["${var.lb-group-source-cidrs}"]
  target_tags   = ["${var.lb-group-target-tags}"]
}

resource "google_compute_instance_template" "lb-backend-instance" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix    = "${var.service}-${var.disk-image}-"
  can_ip_forward = "${var.ip-forward}"
  machine_type   = "${var.machine-type}"
  region         = "${var.gce-region}"

  tags = [
    "${var.envname}",
    "${var.service}",
    "${var.firewall-tags}"
  ]

  disk {
    device_name  = "${var.disk-device-name}"
    source_image = "${var.disk-image}"
    boot         = true
  }

  network_interface {
    subnetwork = "${var.subnetwork-name}"
    access_config {
      # Ephemeral
    }
  }

  metadata {
    envname            = "${var.envname}"
    profile            = "${var.service}"
    domain             = "${var.dns-zone-dns-name}"
    startup-script-url = "${var.startup-script}"
  }

  service_account {
    scopes = ["${var.service-account-scopes}"]
  }
}

resource "google_compute_address" "lb-ip" {
  name = "${var.service}-lb-ip"
}

resource "google_compute_http_health_check" "lb-target-health-check" {
  name               = "${var.service}-healthcheck-${var.gce-region}"
  request_path       = "/"
  check_interval_sec = "${var.lb-backend-health-check-interval}"
  timeout_sec        = "${var.lb-backend-health-check-timeout}"
}

resource "google_compute_target_pool" "lb-instances-pool" {
  name = "${var.service}-lb-pool"

  health_checks = [
    "${google_compute_http_health_check.lb-target-health-check.name}",
  ]
}

resource "google_compute_instance_group_manager" "lb-backend-group-manager" {
  count              = "${var.zone_counter}"
  name               = "${var.service}-instance-group-${count.index}"
  base_instance_name = "${var.service}"
  instance_template  = "${google_compute_instance_template.lb-backend-instance.self_link}"
  update_strategy    = "RESTART"
  zone               = "${element(split(",", lookup(var.gce-zones, var.gce-region)), count.index)}"

  target_pools = [
    "${google_compute_target_pool.lb-instances-pool.self_link}",
  ]
}

resource "google_compute_autoscaler" "lb-backend-scaler" {
  count  = "${var.zone_counter}"
  name   = "${var.service}-autoscaler-${count.index}"
  zone   = "${element(split(",", lookup(var.gce-zones, var.gce-region)), count.index)}"
  target = "${element(google_compute_instance_group_manager.lb-backend-group-manager.*.self_link, count.index)}"

  autoscaling_policy = {
    max_replicas    = "${var.lb-backend-scaler-max}"
    min_replicas    = "${var.lb-backend-scaler-min}"
    cooldown_period = "${var.lb-backend-scaler-cooldown}"

    cpu_utilization {
      target = "${var.lb-backend-scaler-util-target}"
    }
  }
}

resource "google_compute_forwarding_rule" "lb-forwarding-rule" {
  name       = "forward-to-${var.service}"
  ip_address = "${google_compute_address.lb-ip.address}"
  target     = "${google_compute_target_pool.lb-instances-pool.self_link}"
  port_range = "${var.lb-forwarding-rule-ports}"
}

resource "google_dns_record_set" "prefixes-and-lb-dns" {
  count = "${length(var.lb-dns-prefixes)}"

  name         = "${var.lb-dns-prefixes[count.index]}.${var.dns-zone-dns-name}"
  managed_zone = "${var.dns-zone-name}"
  type         = "A"
  ttl          = "300"

  rrdatas = [
    "${google_compute_address.lb-ip.address}",
  ]
}
